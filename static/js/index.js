
$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "/getData/Batman",
        success: function (result) {
            $("#demo").empty();
            $("#demo").append(
                "<tr>" +
                "<td>Picture</td>" +
                "<td>Title</td>" +
                "<td>Authors</td>" +
                "</tr>"
            );
            for (i = 0; i < result.items.length; i++) {
                $("#demo").append(
                    "<tr>" +
                    "<td> <img src = '" + result.items[i].volumeInfo.imageLinks.thumbnail + "'> </td>" +
                    "<td>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                    "</tr>");
            }
        }
    })

    $(".carigan").click(function () {
        var input = $(".inputan").val()
        $.ajax({
            method: "GET",
            url: "/getData/" + input,
            success: function (result) {
                $("#demo").empty();
                $("#demo").append(
                    "<tr>" +
                    "<td>Picture</td>" +
                    "<td>Title</td>" +
                    "<td>Authors</td>" +
                    "</tr>"
                );
                for (i = 0; i < result.items.length; i++) {
                    $("#demo").append(
                        "<tr>" +
                        "<td> <img src = '" + result.items[i].volumeInfo.imageLinks.thumbnail + "'> </td>" +
                        "<td>" + result.items[i].volumeInfo.title + "</td>" +
                        "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                        "</tr>");
                }
            }
        })
    });
});