from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import landing

# Create your tests here.
class story_8_test(TestCase):
    def test_url_is_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)

    def test_using_landingPage_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_using_landing_funct(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_ada_tulisan_cari_buku(self):
        request = HttpRequest()
        response = landing(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Find Your Book!', html_response)
