from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.
def landing(request):
    return render(request, 'landingPage.html')

def get_data(request, search):
    r = requests.get("https://www.googleapis.com/books/v1/volumes?q="+search)
    return JsonResponse(r.json())
